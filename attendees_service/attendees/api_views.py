from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.core.exceptions import FieldDoesNotExist, ValidationError

import json
from common.json import ModelEncoder
from common.helper_functions import validate_fields
from .models import Attendee, ConferenceVO


class ConferenceVOEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        "name",
        "import_href",
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)
        message = validate_fields(content, name=str, email=str)
        if message:
            return JsonResponse(
                {"status": "400", "message": message},
                status=400,
            )
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Conference does not exist"},
                status=404,
            )
        try:
            attendee = Attendee.objects.create(**content)
        except TypeError as e:
            return JsonResponse(
                {"status": "400", "message": str(e)},
                status=400
            )
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        try:
            attendee = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Attendee does not exist"},
                status=404,
            )
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Attendee.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Attendee does not exist"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        # validate the conference
        if content.get("conference") or isinstance(content.get("conference"), bool):
            message = validate_fields(content, conference=int)
            if message:
                return JsonResponse(
                    {"status": "400", "message": message},
                    status=400,
                )
            try:
                conference_href = f"/api/conferences/{content['conference']}/"
                conference = ConferenceVO.objects.get(import_href=conference_href)
                content["conference"] = conference
            except ConferenceVO.DoesNotExist:
                return JsonResponse(
                    {"status": "404", "message": "Conference does not exist"},
                    status=404,
                )
            except ValueError:
                return JsonResponse(
                    {"status": "400", "message": "Invalid input for Conference"},
                    status=400,
                )
        # then try to update the attendee
        try:
            updated_count = Attendee.objects.filter(id=id).update(**content)
        except FieldDoesNotExist as e:
            return JsonResponse(
                {"status": "400", "message": str(e)},
                status=400,
            )
        if updated_count == 0:
            return JsonResponse(
                {"status": "404", "message": "Attendee does not exist"},
                status=404
            )
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
