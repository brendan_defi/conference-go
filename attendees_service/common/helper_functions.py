


def validate_fields(data, **kwargs):
    message = ""
    for key in kwargs:
        value = data.get(key)
        if isinstance(value, bool) and kwargs[key] != bool:
            if not message:
                message += f"Invalid input for {key}"
            else:
                message += f", Invalid input for {key}"
        elif not value:
            if not message:
                message += f"{key} is a required field"
            else:
                message += f", {key} is a required field"
        elif not isinstance(value, kwargs[key]):
            if not message:
                message += f"Invalid input for {key}"
            else:
                message += f", Invalid input for {key}"
    return message
