import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_image(city, state):
    city = city.replace(" ", "+")
    url = f"https://api.pexels.com/v1/search?query={city}&{state}&per_page=1"
    headers = {
        "Authorization": PEXELS_API_KEY
    }

    response = requests.get(url, headers=headers)
    parsed_response = json.loads(response.text)
    try:
        url = parsed_response["photos"][0]["url"]
        desc = parsed_response["photos"][0]["alt"]
    except IndexError:
        url = desc = None
    location_image = {
        "picture_url": url,
        "picture_description": desc,
    }
    return location_image #, location_image_description


def get_coordinates_from_location(city, state):
    city = city.replace(" ", "+")
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    parsed_response = json.loads(response.text)
    try:
        lat = parsed_response[0]["lat"]
        lon = parsed_response[0]["lon"]
    except IndexError:
        lat = lon = None
    return lat, lon


def get_weather_from_coordinates(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    parsed_response = json.loads(response.text)
    try:
        temp = parsed_response["main"]["temp"]
        description = parsed_response["weather"][0]["description"]
    except KeyError:
        temp = description = None
    weather = {
        "temp": temp,
        "decription": description,
    }
    return weather


def get_weather_from_location(city, state):
    lat, lon = get_coordinates_from_location(city, state)
    weather = get_weather_from_coordinates(lat, lon)
    return weather
