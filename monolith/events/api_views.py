from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.core.exceptions import FieldDoesNotExist, ValidationError
import json
from common.json import ModelEncoder
from common.helper_functions import validate_fields
from .models import Conference, Location, State
from .acls import get_location_image, get_weather_from_location


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
    ]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url"
    ]

    def get_extra_data(self, obj):
        return { "state": obj.state.abbreviation }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name"
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        message = validate_fields(
            content,
            name=str,
            starts=str,
            ends=str,
            description=str,
            max_presentations=int,
            max_attendees=int,
            location=int,
        )
        if message:
            return JsonResponse(
                {"status": "400", "message": message},
                status=400,
            )
        # if they are, we move on to additional checks
        else:
            # we next check if the location exists
            try:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
            except Location.DoesNotExist:
                return JsonResponse(
                    {"status": "400", "message": "Location does not exist"},
                    status=400,
                )
            try:
                conference = Conference.objects.create(**content)
            # check for ValidationError (likely in dates)
            except ValidationError as e:
                return JsonResponse(
                    {"status": "400", "message": str(e)},
                    status=400,
                )
            # check for TypeError (likely when the input includes any invalid fields)
            except TypeError as e:
                return JsonResponse(
                    {"status": "400", "message": str(e)},
                    status=400,
                )
        # if we pass all of the error checks, we return a valid response
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        try:
            conference = Conference.objects.get(id=id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Conference does not exist"},
                status=404,
            )
        weather = get_weather_from_location(conference.location.city, conference.location.state)
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Conference.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Conference.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Conference does not exist"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        # check to see if the PUT changes the location
        if content.get("location") or isinstance(content.get("location"), bool):
            if isinstance(content.get("location"), bool):
                return JsonResponse(
                    {"status": "400", "message": "Invalid input for Location"},
                    status=400,
                )
            try:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
            except Location.DoesNotExist:
                return JsonResponse(
                    {"status": "400", "message": "Location does not exist"},
                    status=400,
                )
        # see if we can update the Conference
        try:
            updated_count = Conference.objects.filter(id=id).update(**content)
        # handle FieldDoesNotExist errors
        except FieldDoesNotExist as e:
            return JsonResponse(
                {"status": "400", "message": str(e)},
                status=400,
            )
        # if we haven't updated a conference...
        if updated_count == 0:
            # then the conference doesn't exist
            return JsonResponse(
                {"status": "404", "message": "Conference does not exist"},
                status=404,
            )
        # otherwise we can pull and return the updated conference
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        message = validate_fields(
            content,
            name=str,
            city=str,
            room_count=int,
            state=str,
        )
        if message:
            return JsonResponse(
                {"status": "400", "message": message},
                status=400,
            )
        else:
            try:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
            except State.DoesNotExist:
                return JsonResponse(
                    {"status": "400", "message": "Invalid state abbreviation"},
                    status=400,
                )
            city = content["city"]
            state = content["state"]
            location_image = get_location_image(city, state)
            content["picture_url"] = location_image["picture_url"]
            try:
                location = Location.objects.create(**content)
            except TypeError as e:
                return JsonResponse(
                    {"status": "400", "message": str(e)},
                    status=400,
                )
            return JsonResponse(
                location,
                encoder=LocationDetailEncoder,
                safe=False,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method == "GET":
        try:
            location = Location.objects.get(id=id)
        except Location.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Location does not exist"},
                status=404,
            )
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Location.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Location.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Location does not exist"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        # check to see if the PUT changes the state
        if content.get("state") or isinstance(content.get("state"), bool):
            # see if the state exists
            try:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
            # if the state doesn't exist, throw an error
            except State.DoesNotExist:
                return JsonResponse(
                    {"status":"400", "message": "Invalid state abbreviation"},
                    status=400,
                )
        # see if we can update the Location
        try:
            updated_count = Location.objects.filter(id=id).update(**content)
        # handle FieldDoesNotExist errors
        except FieldDoesNotExist as e:
            return JsonResponse(
                {"status": "400", "message": str(e)},
                status=400,
            )
        # if we haven't updated a location...
        if updated_count == 0:
            # then we know the location doesn't exist and we return an error
            return JsonResponse(
                {"status": "404", "message": "Location does not exist"},
                status=404,
            )
        # otherwise we can pull and return the updated location
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
