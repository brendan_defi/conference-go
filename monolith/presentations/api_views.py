from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.core.exceptions import FieldDoesNotExist, ValidationError
import json
from common.json import ModelEncoder
from common.helper_functions import validate_fields
from .models import Presentation, Status
from events.models import Conference


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status",
    ]

    def get_extra_data(self, obj):
        return {"status": obj.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
    ]

    def get_extra_data(self, obj):
        return {"status": obj.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        message = validate_fields(
            content,
            presenter_name=str,
            company_name=str,
            presenter_email=str,
            title=str,
            synopsis=str,
        )
        if message:
            return JsonResponse(
                {"status": "400", "message": message},
                status=400,
            )
        # we next check if the conference exists
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"status": "400", "message": "Conference does not exist"},
                status=400,
            )
        try:
            presentation = Presentation.create_presentation(**content)
        # check for TypeError (likely when the input includes any invalid fields)
        except TypeError as e:
            return JsonResponse(
                {"status": "400", "message": str(e)},
                status=400,
            )
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Presentation.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"status": "404", "message": "Presentation does not exist"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        # check if the PUT changes the conference
        if content.get("conference") or isinstance(content.get("conference"), bool):
            if isinstance(content.get("conference"), bool):
                return JsonResponse(
                    {"status": "400", "message": "Invalid input for conference"},
                    status=400,
                )
            try:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
            except Conference.DoesNotExist:
                return JsonResponse(
                    {"status": "400", "message": "Conference does not exist"},
                    status=400,
                )
        # see if we can update the Presentation
        try:
            updated_count = Presentation.objects.filter(id=id).update(**content)
        except FieldDoesNotExist as e:
            return JsonResponse(
                {"status": "400", "message": str(e)},
                status=400,
            )
        if updated_count == 0:
            return JsonResponse(
                {"status": "404", "message": "Conference does not exist"},
                status=404,
            )
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
